# Instance settings.
ami = "ami-d38a4ab1"
instance_type = "t2.small"
count = 1

# SSH connection user.
ssh_user = "ubuntu"
ssh_timeout = "2m"

# Empire Ansible Role
empire_ansible_role_folder = "../ansible/roles/empire/files"

# Fastly domain
fastly_front_domain = "https://nytimes.com:443"
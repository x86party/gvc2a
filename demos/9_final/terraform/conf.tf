# Basic Provisioner details.
variable "ami" {}
variable "instance_type" {}
variable "count" {}

# SSH connection details for provisioners.
variable "ssh_user" {}
variable "ssh_timeout" {}

# Ansible details that Terraform controls. 
variable "empire_ansible_role_folder" {}

# Fastly configuration details.
variable "fastly_front_domain" {}
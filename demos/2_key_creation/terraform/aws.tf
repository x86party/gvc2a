#################################################################
# Provider
# 	Tell Terraform that we're working in the AWS Sydney region.
#################################################################
provider "aws" {
  region = "ap-southeast-2"
  profile = "bsides"
}


#################################################################
# AWS Instance
# 	Configuration for our EC2 instance.
#################################################################
resource "aws_instance" "bsides" {

	# The following is the bare-bones of what'll we need to define 
	# an instance.

	# Declare type of image we want.
	ami = "ami-d38a4ab1" 																
	# Declate the size of the image we want.
	instance_type = "t2.small" 																		
	# Declare how many images we want.  
	count = "1"
	# Use our dynamically generated keypair to connect to the box.
	key_name = "${aws_key_pair.tf_ssh_keypair.key_name}" 		
}


#################################################################
# TLS Private Key
# 	Create a new ephemeral RSA private key for provisioning.
#################################################################
resource "tls_private_key" "tf_ssh_key" {
    algorithm = "RSA"
    rsa_bits = 4096
}

# Save that key for later.
resource "local_file" "private_ssh_key" {
    content = "${tls_private_key.tf_ssh_key.private_key_pem}"
    filename = "../ssh/key.pem"
}


#################################################################
# AWS Key Pair
# 	Create an AWS keypair object. We now have an RSA keypair 
# 	object that we can use to interact with our instances 
# (note: you could also use a key) from a file on disk.
#################################################################
resource "aws_key_pair" "tf_ssh_keypair" {
    key_name = "terraform_rsa_key"
    public_key = "${tls_private_key.tf_ssh_key.public_key_openssh}"
}
#################################################################
# Provider
# 	Tell Terraform that we're working in the AWS Sydney region.
#################################################################
provider "aws" {
  region = "ap-southeast-2"
  profile = "bsides"
}

#################################################################
# AWS Instance
# 	Configuration for our EC2 instance.
#################################################################
resource "aws_instance" "bsides" {

	# The following is the bare-bones of what'll we need to define 
	# an instance.

	# Declare type of image we want.
	ami = "ami-d38a4ab1" 																
	# Declate the size of the image we want.
	instance_type = "t2.small" 																		
	# Declare how many images we want.  
	count = "1"
}
#################################################################
# Provider
# 	Tell Terraform that we're working in the AWS Sydney region.
#################################################################
provider "aws" {
  region = "ap-southeast-2"
  profile = "bsides"
}


#################################################################
# AWS Instance
# 	Configuration for our EC2 instance.
#################################################################
resource "aws_instance" "bsides" {

	# The following is the bare-bones of what'll we need to define 
	# an instance.

	# Declare type of image we want.
	ami = "ami-d38a4ab1" 																
	# Declate the size of the image we want.
	instance_type = "t2.small" 																		
	# Declare how many images we want.  
	count = "1"
	# Use our dynamically generated keypair to connect to the box.
	key_name = "${aws_key_pair.tf_ssh_keypair.key_name}" 		
	# Attach some security groups to our guy directly.
	security_groups = ["${aws_security_group.pse_security_group.name}"]

	# Define how Terraform connects to the box (we'll use this below).
	connection {
		# We connect to this instance via SSH.
		type  = "ssh" 																				
		# We're connecting to the server's public IP.
		host = "${self.public_ip}" 													
		# Default user for this AMI is "ubuntu".
		user = "ubuntu" 																			
		# Why not.
		timeout = "2m" 																					
		# This is the key we dynamically created above! ^^^
		private_key = "${tls_private_key.tf_ssh_key.private_key_pem}" 
	}

  # We can run commands on the box using the connection object we defined above!
	provisioner "remote-exec" {
		inline = [
			"echo 'bS1d3Z' > /home/ubuntu/proof.txt",
			"sudo apt-get update -y",
			"sudo apt-get install python -y"
		]
	}

	# Drop the IP of the box into the Ansible inventory.
	provisioner "local-exec" {
		command = "echo '[empire]\n${self.public_ip}' > ../ansible/inventory.txt"
	}
	
	# We run some local commands to set up our SSH key.
	provisioner "local-exec" {
    	command = "chmod 600 ../ssh/key.pem"
  }

	# Create the resource file for the Empire listener automatically.
	# EOT trick from https://groups.google.com/forum/#!topic/terraform-tool/fo7P0ez-ymw.
	provisioner "local-exec" {
		command = <<EOT
			echo 'listeners' > ../ansible/roles/empire/files/agent.rc;
			echo 'uselistener http' >> ../ansible/roles/empire/files/agent.rc;
			echo 'set Host https://nytimes.com:443' >> ../ansible/roles/empire/files/agent.rc;
			echo 'set DefaultProfile /admin/login.php,/console/dashboard.asp,/news/today.jsp| mozilla/5.0 (windows nt 6.1; wow64; trident/7.0);|host: ${aws_instance.bsides.0.public_dns}' >> ../ansible/roles/empire/files/agent.rc;
			echo 'set Name bsides' >> ../ansible/roles/empire/files/agent.rc;
			echo 'execute' >> ../ansible/roles/empire/files/agent.rc;
		EOT
	}
}


#################################################################
# TLS Private Key
# 	Create a new ephemeral RSA private key for provisioning.
#################################################################
resource "tls_private_key" "tf_ssh_key" {
    algorithm = "RSA"
    rsa_bits = 4096
}

# Save that key for later.
resource "local_file" "private_ssh_key" {
    content = "${tls_private_key.tf_ssh_key.private_key_pem}"
    filename = "../ssh/key.pem"
}


#################################################################
# AWS Key Pair
# 	Create an AWS keypair object. We now have an RSA keypair 
# 	object that we can use to interact with our instances 
# (note: you could also use a key) from a file on disk.
#################################################################
resource "aws_key_pair" "tf_ssh_keypair" {
    key_name = "terraform_rsa_key"
    public_key = "${tls_private_key.tf_ssh_key.public_key_openssh}"
}


#################################################################
# Security Groups
# 	Firewall rules that AWS needs to talk to anything.
#################################################################
resource "aws_security_group" "pse_security_group" {
  name = "pse_security_group"
  description = "Allow SSH in, HTTP in, anything out."

	# Allowing SSH in from anywhere (perhaps not what you want IRL!)
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
  }

	# Allowing HTTP in from anywhere (perhaps not what you want IRL!)
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
  }

  # Allowing anything out, for now.
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}


#################################################################
# Output
# 	Print select data about the stuff we just created.
#################################################################
output "bsides.empire" {
	value = [ 
		"${aws_instance.bsides.0.public_ip}",
		"${aws_instance.bsides.0.public_dns}"
	]
}

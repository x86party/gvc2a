#################################################################
# Provide your API key via FASTLY_API_KEY environment variable.
#################################################################

#################################################################
# Resource
# 	We're creating a Fastly servivce that will forward traffic to
#   our C2 instance, allowing us to front HTTPS C2 traffic 
#   through reputable domains.
#################################################################
resource "fastly_service_v1" "empire" {
  name = "Empire C2 Domain Fronting Service"
  force_destroy = true

  domain {
    name    = "${aws_instance.bsides.0.public_dns}"
    comment = "Auto-generated DNS name for Empire C2 instance."
  }

  backend {
    address = "${aws_instance.bsides.0.public_dns}"
    name    = "Empire C2 Server"
    port    = 80
  }

  cache_setting {
      name  = "Disable Caching"
      action = "pass"
  }
}
